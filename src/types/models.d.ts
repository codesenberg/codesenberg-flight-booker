/**
 * Codesenberg Flight Booker model definitions
 */

declare interface ApiRequestState {
  errorStatus: number;
  isLoading: boolean;
  response: any[];
}

declare interface TestState {
  status: string;
}

declare interface PassengerDropdown {
  id: string;
  label: string;
}

declare interface AirportData {
  altitude: number;
  city: string;
  country: string;
  dst: string;
  iata: string;
  icao: string;
  latitude: number;
  longitude: number;
  name: string;
  timezone: number;
}

//Form Events
declare interface FormEvent {
  preventDefault: () => any;
}

declare interface ChangeEvent {
  target: any;
}

declare interface KeyboardChangeEvent {
  keyCode: number;
}