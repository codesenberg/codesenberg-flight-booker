import * as React from 'react';
import * as style from '../styles/style.css';
import { ResultItem } from './result-item';

declare namespace Results {
  export interface Props {
     data: any;
     isLoading: boolean;
     errorStatus: number;
  }

  /*export interface State {
  }*/
}

export class Results extends React.Component<Results.Props, {}> {
  constructor(props?: Results.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
  }

  public render(): any {
    let viewState;

    if (!this.props.isLoading) {
      //if results are not loading or has completed loading
      if (this.props.errorStatus === 0) {
        //if results loaded and there are no errors
        viewState = (
            <section>
              <br/>
              <br/>
              <span className={style.results}>There are {this.props.data.length} result(s)</span>
              <br/>
              <br/>
              <table>
                <tbody>
                {this.props.data.map((item, i) =>
                  <tr key={item.id}>
                    <td>
                      <ResultItem key={item.id} slice={item.slice} saleTotal={item.saleTotal} itemIndex={i + 1}/>
                    </td>
                  </tr>
                )}
                </tbody>
              </table>
            </section>
          );
      } else {
        //if results loaded with errors
        viewState = (
        <div className={style.error}>
          There was an error. Please try your search again with valid entries.
        </div>
      );
      }
    } else {
      //if results are loading
      viewState = (
        <div>
          Loading...
        </div>
      );
    }
    return viewState;
  }

  /**
   * @desc configuring all function bindings here
   * @param scope defines context
   */
  private _configBindings(scope: any): void {

  }

}