/**
 * Simple dropdwn componet which renders json data from an array and triggers change events
 */
import * as React from 'react';

declare namespace DropdownComponent {
  export interface Props {
    data: PassengerDropdown[];
    onChange: (event: ChangeEvent) => void;
    name: string;
  }
}

export class DropdownComponent extends React.Component<DropdownComponent.Props, {}> {
  constructor(props?: DropdownComponent.Props, context?: any) {
    super(props, context);
  }

  public render(): any {
    return (
    <select onChange={this.props.onChange} name={this.props.name}>
      {this.props.data.map((item) => <option key={item.id} value={item.id}>{item.label}</option>)}
    </select>
    );
  }
}
