/**
 * Simple dropdwn componet which renders json data from an array and triggers change events
 */
import * as React from 'react';
import * as style from '../styles/style.css';
declare namespace ErrorTextComponent {
  export interface Props {
    isError: boolean;
    errorMessage: string;
  }
}

export class ErrorTextComponent extends React.Component<ErrorTextComponent.Props, {}> {
  constructor(props?: ErrorTextComponent.Props, context?: any) {
    super(props, context);
  }

  public render(): any {
    let errorText;

    if (this.props.isError) {
      errorText = (
        <div className={style.error}>{this.props.errorMessage}</div>
      );
    } else {
      errorText = null;
    }
    return errorText;
  }
}
