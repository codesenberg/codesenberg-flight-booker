import { handleActions } from 'redux-actions';
import * as Actions from '../constants';

const initialState: ApiRequestState[] = [{
  errorStatus: 0,
  isLoading: false,
  response: []
}];

export const apiHandlerReducer = handleActions<ApiRequestState[], ApiRequestState>({
  [Actions.LOAD]: (state, action) => {
    return [{
      isLoading : true
    }];
  },
  [Actions.COMPLETE]: (state, action) => {
    return [{
      isLoading : false,
      errorStatus: action.payload.errorStatus,
      response: action.payload.response
    }];
  }
}, initialState);