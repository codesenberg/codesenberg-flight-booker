# Codsenberg Fight Booker App

Simple flight booking app built with React/Redux/Typescript

## Instructions on how to get it running

```
$ git clone https://codesenberg@bitbucket.org/codesenberg/codesenberg-flight-booker.git
$ cd codesenberg-flight-booker
$ npm install
# make sure port 3000 is free
$ npm start
# this will load the app in your desired web browser with the following url: http://localhost:3000/
```

## Testing

```
$ npm test
```

## Contains

- [x] [Typescript](https://www.typescriptlang.org/) 2.4.1
- [x] [React](https://facebook.github.io/react/) 15.6
- [x] [Redux](https://github.com/reactjs/redux) 3.7
- [x] [React Router](https://github.com/ReactTraining/react-router) 4.1
- [x] [Redux DevTools Extension](https://github.com/zalmoxisus/redux-devtools-extension)
- [x] [TodoMVC example](http://todomvc.com)

### Build tools

- [x] [Webpack](https://webpack.github.io) 3.0
  - [x] [Tree Shaking](https://medium.com/@Rich_Harris/tree-shaking-versus-dead-code-elimination-d3765df85c80)
  - [x] [Webpack Dev Server](https://github.com/webpack/webpack-dev-server)
- [x] [Awesome Typescript Loader](https://github.com/s-panferov/awesome-typescript-loader)
- [x] [PostCSS Loader](https://github.com/postcss/postcss-loader)
  - [x] [CSS next](https://github.com/MoOx/postcss-cssnext)
  - [x] [CSS modules](https://github.com/css-modules/css-modules)
- [x] [React Hot Loader](https://github.com/gaearon/react-hot-loader)
- [x] [ExtractText Plugin](https://github.com/webpack/extract-text-webpack-plugin)
- [x] [HTML Webpack Plugin](https://github.com/ampedandwired/html-webpack-plugin)

# License

MIT
