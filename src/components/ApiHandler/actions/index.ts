import { createAction } from 'redux-actions';
import * as Actions from '../constants';

export const requestLoad = createAction<ApiRequestState>(Actions.LOAD);
export const requestComplete = createAction<ApiRequestState>(Actions.COMPLETE);
