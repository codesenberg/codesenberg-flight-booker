import * as moment from 'moment';
import * as React from 'react';
import DatePicker from 'react-datepicker';

declare namespace DatePickerWrapper {
  export interface Props {
    onChange: (event: ChangeEvent) => void;
    name: string;
    format: string;
    startDate: string;
  }

  export interface State {
    startDate: any;
  }
}

export class DatePickerWrapper extends React.Component<DatePickerWrapper.Props, DatePickerWrapper.State> {
  private _textInput: HTMLInputElement;
  constructor(props?: DatePickerWrapper.Props, context?: any) {
    super(props, context);
    this.state = {
      startDate: this.props.startDate
    };
    this._configBindings(this);
  }

  public render(): any {
    return (
        <DatePicker
          selected={this.state.startDate}
          onChange={this._handleInputChange}
          onMouseDown={this.test}
        />
    );
  }

  public test(): void {
    console.log('dit this??')
  }

  /**
   * @desc handles input change event
   * @param event defines change event
   */
  private _handleInputChange(date: any): void {
    const changeEvent: ChangeEvent = {target: {name: this.props.name, value: date.format(this.props.format)}};
    this.setState({
      startDate: date
    });
    this.props.onChange(changeEvent);
  }
  /**
   * @desc configuring all function bindings here
   * @param scope defines context
   */
  private _configBindings(scope: any): void {
    this._handleInputChange = this._handleInputChange.bind(scope);
    this.test = this.test.bind(scope);
  }
}
