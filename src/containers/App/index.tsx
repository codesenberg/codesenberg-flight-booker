import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { bindActionCreators } from 'redux';
import { ApiHandler, ApiHandlerActions, BookingFormComponent, Results } from '../../components';
import { RootState } from '../../root-reducer';
import * as style from './styles/style.css';
const airportsData = require('./data/airports.json');

declare namespace App {
  export interface Props extends RouteComponentProps<void> {
    actions: typeof ApiHandlerActions;
    apiHandlerState: ApiRequestState;
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export class App extends React.Component<App.Props, {}> {
  private _airportsData: any[] = Object.values(airportsData);
  constructor(props?: App.Props, context?: any) {
    super(props, context);
  }

  render() {
    const {apiHandlerState, actions, children } = this.props;
    return (
      <section>
        <h1 className={style.h1}>Codesenberg Flight Search App</h1>
        <br/>
        <br/>
        <BookingFormComponent airportData={this._airportsData} apiActions={actions} />
        <Results data={apiHandlerState[0].response} errorStatus={apiHandlerState[0].errorStatus} isLoading={apiHandlerState[0].isLoading} />
      </section>
    );
  }
}

function mapStateToProps(state: RootState) {
  return {
    apiHandlerState: state.apiHandlerReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ApiHandlerActions as any, dispatch)
  };
}