export class ApiHandler {

  /**
   * @desc handles Post api request and formatted for json
   * @param url defines the url
   * @param requestData defines the request data
   * @param action defines redux action to invoke
   */
  static request(url: string, requestData: any, action: () => any): Promise<any> {
    action();
    return fetch(url, {
        method: 'POST',
        headers: new Headers({
          'Content-Type': 'application/json'
        }),
        body: JSON.stringify(requestData)
      }).then((response) => {
        return response.json();
    });
  }

}
