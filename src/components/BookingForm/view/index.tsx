import * as moment from 'moment';
import * as React from 'react';
import { ApiHandler } from '../../ApiHandler';
import { DatePickerWrapper } from '../../DatePickerWrapper';
import { DropdownComponent } from '../../Dropdown';
import { ErrorTextComponent } from '../../ErrorText';
import { PredictiveTextInput } from '../../PredictiveTextInput';

declare namespace BookingFormComponent {
  export interface Props {
    airportData: any;
    apiActions: any;
  }

  export interface State {
    adult: number;
    children: number;
    infants: number;
    origin: string;
    destination: string;
    departureDate: string;
    returnDate: string;
    errors: any;
  }
}

export class BookingFormComponent extends React.Component<BookingFormComponent.Props, BookingFormComponent.State> {
  private _parentDropdown: PassengerDropdown[];
  private _childDropdown: PassengerDropdown[];
  private _datePickerFormat: string = 'YYYY-MM-DD';
  private _startDateObj: any = moment();
  private _startDateObj2: any = moment().add(1, 'day');
  private _apiUrl = 'https://www.googleapis.com/qpxExpress/v1/trips/search?key=AIzaSyBh5btiJ-bZWJruCCtROPlUo20nldl3nHU';
  constructor(props?: BookingFormComponent.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    this._parentDropdown = this._configurePassengerDropdowns(1, 10);
    this._childDropdown = this._configurePassengerDropdowns(0, 10);
    this.state = {
      adult: 1,
      children: 0,
      infants: 0,
      origin: '',
      destination: '',
      departureDate: this._startDateObj.format(this._datePickerFormat),
      returnDate: this._startDateObj.format(this._datePickerFormat),
      errors: {
        destination: {
          isError: false,
          message: ''
        },
        origin: {
          isError: false,
          message: ''
        },
        departureDate: {
          isError: false,
          message: ''
        },
        returnDate: {
          isError: false,
          message: ''
        },
        isValid: true
      }
    };
  }

  public render(): any {
    return (
      <form onSubmit={this._handleSubmit}>
        <div>Tip: as you enter Depart and Return fields, you may select an item from the list of returned destinations</div>
        <br/>
        <PredictiveTextInput data={this.props.airportData} dataLimit={10} label="Depart From" name="origin" onChange={this._handleInputChange} />
        <ErrorTextComponent isError={this.state.errors.origin.isError} errorMessage={this.state.errors.origin.message} />
        <PredictiveTextInput data={this.props.airportData} dataLimit={10} label="Return From" name="destination" onChange={this._handleInputChange} />
        <ErrorTextComponent isError={this.state.errors.destination.isError} errorMessage={this.state.errors.destination.message} />
        <br/>
        <label>
          Departure Date:
        </label>
        <DatePickerWrapper startDate={this._startDateObj} name="departureDate" onChange={this._handleInputChange} format={this._datePickerFormat}/>
        <ErrorTextComponent isError={this.state.errors.departureDate.isError} errorMessage={this.state.errors.departureDate.message} />
        <br/>
        <label>
          Return Date:
        </label>
        <DatePickerWrapper startDate={this._startDateObj2} name="returnDate" onChange={this._handleInputChange} format={this._datePickerFormat}/>
        <ErrorTextComponent isError={this.state.errors.returnDate.isError} errorMessage={this.state.errors.returnDate.message} />
        <br/>
         Passengers:
         <br/>
        <label>
          Adults&nbsp;&nbsp;
          <DropdownComponent data={this._parentDropdown} onChange={this._handleInputChange} name="adult" />
        </label>
        <label>
          &nbsp;Children&nbsp;&nbsp;
          <DropdownComponent data={this._childDropdown} onChange={this._handleInputChange} name="children" />
        </label>
        <label>
          &nbsp;Infants&nbsp;&nbsp;
          <DropdownComponent data={this._childDropdown} onChange={this._handleInputChange} name="infants" />
        </label>
        <br/>
        <br/>
        <input type="submit" value="Submit" />
      </form>
    );
  }

  /**
   * @desc configuring all function bindings here
   * @param scope defines context
   */
  private _configBindings(scope: any): void {
    this._handleInputChange = this._handleInputChange.bind(scope);
    this._handleSubmit = this._handleSubmit.bind(scope);
  }

  /**
   * @desc handles input change event
   * @param event defines change event
   */
  private _handleInputChange(event: ChangeEvent): void {
    const target = event.target;
    const value = (target.valueParsed) ? target.valueParsed : target.value;
    const name = target.name;
    this.setState({
      [name]: value
    });
    this._checkForErrors();
  }

  /**
   * @desc handles submiting the form if valid
   * @param event defines FormEvent
   */
  private _handleSubmit(event: FormEvent): void {
    event.preventDefault();
    console.log(this.state, 'what is my state??');

    if (!this._checkForErrors()) {
      const requestData = {
          request: {
            passengers: {
              adultCount: this.state.adult,
              childCount: this.state.children,
              infantInSeatCount: this.state.infants
            },
            slice: [
              {
                origin: this.state.origin,
                destination: this.state.destination,
                date: this.state.departureDate
              },
              {
                origin: this.state.destination,
                destination: this.state.origin,
                date: this.state.returnDate
              }
            ]
        }
      };

      ApiHandler.request(this._apiUrl, requestData, this.props.apiActions.requestLoad).then((data) => {
        console.log(data, 'api response');
        this.props.apiActions.requestComplete({isLoading: true, errorStatus: 0, response: data.trips.tripOption});
      }).catch((error) => {
        console.log('There has been a problem with your fetch operation: ' + error);
        this.props.apiActions.requestComplete({isLoading: false, errorStatus: 1, response:[]});
      });
    }
  }

  /**
   * @desc configures passenger dropdowns based on passed in indexes
   * @param startIndex defines start index
   * @param endIndex defines end index
   * @return dropdown data
   */
  private _configurePassengerDropdowns(startIndex: number, endIndex: number): PassengerDropdown[] {
    const tempData = [];
    for (let i = startIndex; i < endIndex; i++) {
      tempData.push({id: i, label: i});
    }
    return tempData;
  }

  /**
   * @desc checks for errors
   * @return true if there are any errors detected
   */
  private _checkForErrors(): boolean {
    const errorObj = this.state.errors;
    const curDate = moment(this._startDateObj.format(this._datePickerFormat));
    const departureDate = moment(this.state.departureDate, this._datePickerFormat);
    const returnDate = moment(this.state.returnDate);
    let isError = false;

    //depature
    if (this.state.origin.trim().length === 0) {
      errorObj.origin.isError = true;
      errorObj.origin.message = 'Please select departure location';
      isError = true;
    } else {
      errorObj.origin.isError = false;
      errorObj.origin.message = '';
    }

    //arrival
    if (this.state.destination.trim().length === 0) {
      errorObj.destination.isError = true;
      errorObj.destination.message = 'Please select arrival location';
      isError = true;
    } else {
      errorObj.destination.isError = false;
      errorObj.destination.message = '';
    }

    /*
    //departure date
    if (!departureDate.isValid()) {
      errorObj.departureDate.isError = true;
      errorObj.departureDate.message = 'Please use date picker and select departure date';
      isError = true;
    } else if (departureDate.isBefore(curDate)) {
      errorObj.departureDate.isError = true;
      errorObj.departureDate.message = 'Date cannot be before today';
      isError = true;
    } else {
      errorObj.departureDate.isError = false;
      errorObj.departureDate.message = '';
    }*/

    errorObj.isValid = !isError;
    this.setState({errors: errorObj});

    return isError;

  }
}
