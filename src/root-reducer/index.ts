import { combineReducers } from 'redux';
import { apiHandlerReducer } from '../components/ApiHandler';

export interface RootState {
    apiHandlerReducer: ApiRequestState[];
}

export default combineReducers<RootState>({
    apiHandlerReducer
});
