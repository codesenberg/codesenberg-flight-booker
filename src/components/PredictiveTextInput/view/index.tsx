import * as React from 'react';
import * as style from '../styles/style.css';

declare namespace PredictiveTextInput {
  export interface Props {
     data: AirportData[];
     dataLimit: number;
     label: string;
     name: string;
     onChange: (event: ChangeEvent) => void;
  }

  export interface State {
    inputText: string;
    valueParsed: string;
    showPredictive: boolean;
  }
}

export class PredictiveTextInput extends React.Component<PredictiveTextInput.Props, PredictiveTextInput.State> {
  constructor(props?: PredictiveTextInput.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    this.state = {inputText: '', showPredictive: false, valueParsed: ''};
  }

  public render(): any {
    let predictiveText;
    if (this.state.showPredictive) {
      predictiveText = (
            <ul className={style.list}>
              {this.props.data
              .filter((item) => this._filterAirportData(item))
              .slice(0, this.props.dataLimit)
              .map((item) => <li key={item.iata} data-value-parsed={item.iata} value={this._formatAirportDataString(item)} onMouseDown={this._handlePredictiveSelect}>{this._formatAirportDataString(item)}</li>)}
            </ul>
          );
    } else {
      predictiveText = null;
    }
    return (
        <label onBlur={this._handleBlurChange}>
          {this.props.label}
          <div>
            <input type="text" name={this.props.name} data-value-parsed={this.state.valueParsed} value={this.state.inputText} onChange={this._handleInputChange} onKeyDown={this._handleKeyDown}  autoComplete="off" />
            {predictiveText}
          </div>
        </label>
    );
  }

  /**
   * @desc configuring all function bindings here
   * @param scope defines context
   */
  private _configBindings(scope: any): void {
    this._handleInputChange = this._handleInputChange.bind(this);
    this._handleBlurChange = this._handleBlurChange.bind(this);
    this._handlePredictiveSelect = this._handlePredictiveSelect.bind(this);
    this._filterAirportData = this._filterAirportData.bind(this);
    this._formatAirportDataString = this._formatAirportDataString.bind(this);
    this._handleKeyDown = this._handleKeyDown.bind(this);
  }

  /**
   * @desc handles updating prediction list as user types
   * @param event defines change event
   */
  private _handleInputChange(event: ChangeEvent): void {
    const target = event.target;
    const value = target.value;

    this.setState({
      inputText: value,
      showPredictive: true
    });
    this.props.onChange(event);
  }

  private _handleKeyDown(event: KeyboardChangeEvent): void {
    if (event.keyCode === 13) {
      this.setState({showPredictive: false});
    }
  }

  /**
   * @desc closes the predictive list
   * @param event defines change event
   */
  private _handleBlurChange(event: ChangeEvent): void {
    this.setState({showPredictive: false});
  }

  /**
   * @desc handles updating text input when user selects from selective list
   * @param event defines change event
   */
  private _handlePredictiveSelect(event: ChangeEvent): void {
    const changeEvent: ChangeEvent = {target: {name: this.props.name, value: event.target.getAttribute('value'), valueParsed: event.target.getAttribute('data-value-parsed')}};
    this.setState({valueParsed: event.target.getAttribute('data-value-parsed')});
    this.setState({inputText: event.target.getAttribute('value') });
    this._handleInputChange(changeEvent);
  }

  /**
   * @desc filters airport data against text entered in input
   * @param item defines airport data
   */
  private _filterAirportData(item: AirportData): boolean {
    const airportString = this._formatAirportDataString(item).toLowerCase();
    return airportString.indexOf(this.state.inputText.toLowerCase()) !== -1;
  }

  /**
   * @desc formats airport data into readable string
   * @param item defines airport data
   */
  private _formatAirportDataString(item: AirportData): string {
     return item.city + ', ' + item.country + ', ' + item.name + ', ' + item.iata;
  }
}
