import * as moment from 'moment';
import * as React from 'react';
import * as style from '../styles/style.css';

declare namespace Results {
  export interface Props {
     itemIndex: number;
     saleTotal: string;
     slice: any[];
  }

  /*
  export interface State {
  }*/
}

export class ResultItem extends React.Component<Results.Props, {}> {
  private _flightType = {0: 'Departures', 1: 'Arrivals'};
  private _datePickerFormat: string = 'YYYY-MM-DD HH:MM';
  constructor(props?: Results.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
  }

  public render(): any {
    return (
      <div>
            <br/>
            <h1 className={style.options}>Option {this.props.itemIndex} - {this.props.saleTotal}</h1>
            {this.props.slice.map((slice, sliceIndex) =>
            <div key={sliceIndex}>
              <br/>
              <span className={style[this._flightType[sliceIndex].toLowerCase()]}>{this._flightType[sliceIndex]}</span>
              <br/>
              <br/>
              {slice.segment.map((segment, segmentIndex) =>
                <div key={segment.id}>
                  <span>Flight {segmentIndex + 1}<br/></span>
                  {segment.leg.map((leg) =>
                    <div key={leg.id}>
                      <div className={style['flight-spacer']}>
                        <label>
                          Arrival Time:&nbsp;
                          {moment(leg.arrivalTime).format(this._datePickerFormat)}
                          </label>
                        <br/>
                        <label>
                          Departure Time:&nbsp;
                          {moment(leg.departureTime).format(this._datePickerFormat)}
                        </label>
                        <br/>
                        <label>
                          Destination:&nbsp;
                          {leg.destination}
                        </label>
                        <br/>
                        <label>
                          Origin:&nbsp;
                          {leg.origin}
                        </label>
                      </div>
                    </div>
                  )}
                </div>
              )}
            </div>
          )}
      </div>
    );
  }

  /**
   * @desc configuring all function bindings here
   * @param scope defines context
   */
  private _configBindings(scope: any): void {

  }

}
